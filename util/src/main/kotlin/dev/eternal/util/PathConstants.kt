package dev.eternal.util

/**
 * A static list of [String] paths for files
 *
 * @author Cody Fullen
 */
object PathConstants {

    const val SERVER_CONFIG_PATH = "data/config/server.properties"

}